/*
# File Created by : Subodh Shetty on 
# This file is used to Read the Config settings for the app.
# The Config File is located in the root (/config.json)
*/


var _config = rekuire('config.json');

function getInstanceMode() {

    var retValue = "prod";

	process.argv.forEach(function(val, index, array) {
  			if(val.split('=')[0] === 'mode')
  			{
  				retValue = val.split('=')[1]
  			}
	});

	 return  retValue ;
}

function getMongoDBConn(key) {

	if(typeof key === "undefined")
	{
		 if(getInstanceMode() === 'prod')
		  {
		  	 return _config.prod.mongodbConn.url;
		  }
		  else if(getInstanceMode() ==='dev')
		  {
		  	 return _config.dev.mongodbConn.url;
		  }
	}
	else
	{
		 if(key === 'prod')
		  {
		  	 return _config.prod.mongodbConn.url;
		  }
		  else if(key ==='dev')
		  {
		  	 return _config.dev.mongodbConn.url;
		  }
	}
}

function getLogFilePath(key){

	if(typeof key === "undefined")
	{
		 if(getInstanceMode() === 'prod')
		  {
		  	 return _config.prod.logFilePath;
		  }
		  else if(getInstanceMode() ==='dev')
		  {
		  	 return _config.dev.logFilePath;
		  }
	}
	else
	{
		 if(key === 'prod')
		  {
		  	 return _config.prod.logFilePath;
		  }
		  else if(key ==='dev')
		  {
		  	 return _config.dev.logFilePath;
		  }
	}
}


function getCollectionName(key){

	if(typeof key === "undefined")
	{
		 if(getInstanceMode() === 'prod')
		  {
		  	 return _config.prod.mongodbConn.coll;
		  }
		  else if(getInstanceMode() ==='dev')
		  {
		  	 return _config.dev.mongodbConn.coll;
		  }
	}
	else
	{
		 if(key === 'prod')
		  {
		  	 return _config.prod.mongodbConn.coll;
		  }
		  else if(key ==='dev')
		  {
		  	 return _config.dev.mongodbConn.coll;
		  }
	}
}


function getPortNo(key){

	if(typeof key === "undefined")
	{
		 if(getInstanceMode() === 'prod')
		  {
		  	 return _config.prod.port;
		  }
		  else if(getInstanceMode() ==='dev')
		  {
		  	 return _config.dev.port;
		  }
	}
	else
	{
		 if(key === 'prod')
		  {
		  	 return _config.prod.port;
		  }
		  else if(key ==='dev')
		  {
		  	 return _config.dev.port;
		  }
	}
}


module.exports.getInstanceMode = getInstanceMode;
module.exports.getMongoDBConn = getMongoDBConn;
module.exports.getLogFilePath = getLogFilePath;
module.exports.getCollectionName = getCollectionName;
module.exports.getPortNo = getPortNo;