var mongo = rekuire('lib/db/mongo');
var _logger= rekuire('logger.js');

function init(app)
{
	app.post('/api/audit', function (req, res) {

		_logger.debugInfo('Inside /api/audit route');		

		 var postContent = req.body;

		 _logger.debugInfo(postContent);
		
		if(req.is('application/json'))
		{
			_logger.debugInfo("inside first block");
			if(typeof postContent != 'undefined')
			{
				_logger.debugInfo('postContent-' + JSON.stringify(postContent)) ;

				
				    mongo.insertLog(postContent,function(err,result){
					if(err)
					{
						// Some Error Occurred. Please log.
						 _logger.debugInfo('Error occurred while inserting the data-\n' + 'Error trace-' + err.stack);
					}
					else
					{
						//Successful execution. Log this as well
						
						 _logger.debugInfo('Sucessful insertion done');
					}

				});

				res.send('true');
			}
			else
			{
				 _logger.debugInfo('Empty JSON posted or undefined JSON obj');
				 res.status(403).end();
			}
		}
		else
			{
				 _logger.debugInfo('JSON not posted to the API');
				 res.status(403).end();
			}

  
});

}


module.exports.init=init;