

var fs = require('fs');
var packageJson = rekuire('package.json');
var _conf = rekuire('configReader.js');

/* To format date time to desired format :- "Date=20 Feb 2013 @ 3:46 PM" */
function getlogdate() {

    var currentTime = new Date();
    var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var date = "Date =" + currentTime.getDate() + " " + month[currentTime.getMonth()] + " " + currentTime.getFullYear();
    var suffix = "AM";
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();

    if (minutes < 10){
        minutes = "0" + minutes;
    }
    if (hours >= 12)
    {
        suffix = "PM";
        hours = hours - 12;
    }
    if (hours == 0)
    {
        hours = 12;
    }
        
    return date + " @ " + hours + ':' + minutes + ' ' + suffix;
}

/* Function to format text which needs to be written in the text file */

function logInfo(errType, source, methodName, transactionId, statement, description, reference)
{
    var appName = packageJson.name;
    var currentTime = new Date();
    var fileName = _conf.getLogFilePath() + _conf.getInstanceMode() + '-' +  errType + '-' + currentTime.getFullYear() + (currentTime.getMonth() + 1).toString() + currentTime.getDate().toString() + '.log';
    
    
    if (!appName){
        appName = "";
    }

    if(!errType){
        errType = "";
    }

    if(!source){
        source = "";
    }

    if(!methodName){
        methodName = "";
    }

    if(!transactionId){
        transactionId = "";
    }

    if(!statement){
        statement = "";
    }

    if(!description){
        description = "";
    }

    if(!reference){
        reference = "";
    }

    var text = '\n\n';
    text += '** ' + getlogdate() + '\n';
    text += 'Type=' + errType + '\n';
    text += 'Application=' + appName + '\n';
    text += 'Source=' + source + '\n';
    text += 'Method=' + methodName + '\n';
    text += 'TransactionId=' + transactionId + '\n';
    text += 'Statement=' + statement + '\n';
    text += 'Description=' + description + '\n';
    text += 'Reference=' + reference + '\n';



    fs.appendFile(fileName, text, function (err) {
      if (err) {
                console.log('Error while logging-%s',text );
        }
    });

}


function debugInfo(msg)
{
    if(_conf.getInstanceMode() == 'dev') {

            var appName = packageJson.name;
            var currentTime = new Date();
            var fileName = _conf.getLogFilePath() + _conf.getInstanceMode() + '-'  + currentTime.getFullYear() + (currentTime.getMonth() + 1).toString() + currentTime.getDate().toString() + '.log';
            

                if(msg !== '') {
                
                 fs.appendFile(fileName, msg + '\n', function (err) {
                  if (err) {
                            console.log('Error while logging-%s',msg );
                    }
                });
             }
         }


}



module.exports.logInfo = logInfo;
module.exports.debugInfo = debugInfo;

