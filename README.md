# Overview #

This Nodejs App is a plug and play web endpoint. it does not need any special configuration.All you need to do id change the connection string of the Mongo DB database.
This was primarily developed for Audit purposes in one of my ASP.NET application.

### 1.  What does this Node JS API do? ###

This NodeJS API, hen started, just listens on the configured port. It takes the JSON posted to it-self and dump it into a Mongo database. It could be any valid JSON. This was 

### 2. How did it help in audit ? ###

This was used in one of my ASP.NET application where whenever any UPDATE / INSERT / DELETE statements were fired to the MSSQL database, the same also needed to be stored for audit purposes, so that wlate if needed, we can answer questions like  what / who and when did  a change happen in the MSSQL database. 

This was done by making the JSON in the ASP.NET application it-self and then synchronously POSTING the data to this NodeJS API, which stored the same in the Mongo DB; which was later used to query to identify who did what change/