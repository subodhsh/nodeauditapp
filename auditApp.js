global.rekuire = require('rekuire');

var express = rekuire('express');
var path = rekuire('path');
var bodyParser = rekuire('body-parser');
var apiroutes = rekuire('routes/audit-api-routes.js');
var _conf = rekuire('configReader.js');
var _logger= rekuire('logger.js');
var _mongocli = require('mongodb');
var app = express();
cluster = rekuire ('cluster');
numCPUs = rekuire('os').cpus().length

if ( cluster.isMaster) {

  for ( var i=0; i<=numCPUs; i++ )
    cluster.fork();    // This will Spins as many threads as there are cores
	

} 

else {

		app.use(bodyParser.json()); // for parsing application/json



		app.get('/', function (req, res) {
		 res.send('The AuditAPI app is Up and running !!');
		});

		apiroutes.init(app);

		// Initialize DB connection once
		 
		_mongocli.MongoClient.connect(_conf.getMongoDBConn(), function(err, database) {

		  if(err) throw err;
		 
		  db = database;
		  coll = db.collection(_conf.getCollectionName());

			  var server = app.listen(_conf.getPortNo(), function () {

			  var host = server.address().address
			  var port = server.address().port

			  console.log('Example app listening at http://%s:%s , running in %s mode', host, port, _conf.getInstanceMode());
				

			})
		 
		  
		});

}


